import { useState, useEffect } from 'react';
import { Button, Row, Col, Form } from "react-bootstrap";
import Swal from 'sweetalert2';
import Router from "next/router";

const CategoryForm = () => {
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    // const [isActive, setIsActive] = useState(true);

    const addCategory = (e) => {
        e.preventDefault();
        
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/categories`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            categoryName: categoryName,
            categoryType: categoryType
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data === true){
                Swal.fire('Sucessfully added the category','',
                        'success'
                        )
            Router.push('/categories')
            } else {
                Swal.fire('Something went wrong!',
                     'Try adding something again.',
                      'error'
                      )
            }
        })
    }

    // console.log(categoryType)
    // useEffect(() => {
    //     if(categoryType === 'Select Category'){
    //         setIsActive(true);
    //     } else {
    //         setIsActive(false);
    //     }
    // },[categoryType])
    return <>
        <Row className="justify-content-center">
            <Col xs md="6">
            <h1>Add New Category</h1>
                <Form onSubmit={(e) => addCategory(e)} className="my-4">
                    <Form.Group>
                        <Form.Label>Category Name</Form.Label>
                        <Form.Control 
                                type="text" 
                                placeholder="Enter Category Name"
                                value={categoryName}
                                onChange={e => setCategoryName(e.target.value)} 
                                required
                            />
                    </Form.Group>
                    
                    <Form.Group>
                        <Form.Label>Category Type</Form.Label>
                        <Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
                            <option selected>Select Category</option>
                            <option>Income</option>
                            <option>Expense</option>
                        </Form.Control>
                    </Form.Group>

                    <Button className="bg-dark" type="submit" block>Submit</Button>
                </Form>
            </Col>
        </Row>
    </>
}

export default CategoryForm



