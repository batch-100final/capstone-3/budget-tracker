
import { Table, Button, Col, Row } from 'react-bootstrap';
import Link from 'next/link'

const Index = ({data}) => {
    console.log(data)
    
    let categoryData = data.map(category => {
            return(
                <tr key={category._id}>
                    <td>{category.categoryName}</td>
                    <td>{category.categoryType}</td>
                </tr>
            )
    })

    return <>
        <Row className="justify-content-center">
            <Col xs md="4">
                <h2 className="text-center">Categories</h2>
                <Link href="/categories/new">
                    <a><Button variant="outline-success" type="submit" block>Add Category</Button></a>
                </Link>
            </Col>
        </Row>

                <Table className="table-striped my-5" hover>
                    <thead className="thead-dark">
                        <tr>
                            <th>Category Name</th>
                            <th>Category Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categoryData}
                    </tbody>
                </Table>
            
    </>
}

export default Index

export async function getStaticProps() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/categories`)
    const data = await res.json();

    return {
        props: {
            data
        }
    }
} 