
import { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { Form, Row, Col } from "react-bootstrap";
import moment from 'moment';


const Trend = ({ data }) => {
    // console.log(data);

    const [ records, setRecords ] = useState([]);
    const [ startDate, setStartDate ] = useState ('');
    const [ endDate, setEndDate ] = useState ('');
    const [ resultDate, setResultDate ] = useState('')
    const [ totalBalance, setTotalBalance ] = useState ([]);

    // let sDate = "(moment(record.createdOn).format(\"YYYY-MM-DD\")"
    
    useEffect(() => {
        let dates = []
        let total = [];
        let amount = 0;

        if (startDate !== '' && endDate !== '') {
            data.forEach(record => {
                if((moment(record.createdOn).format("YYYY-MM-DD")) >= startDate && (moment(record.createdOn).format("YYYY-MM-DD")) <= endDate) {
                    if(record.categoryType === "Income") {
                        // console.log(record.categoryName)
                        amount = amount + record.amount
                        total.push(amount)  
                        // console.log(amount)
                    }
                    if(record.categoryType === "Expense") {
                        // console.log(record.categoryName)
                        amount = amount - record.amount
                        total.push(amount)
                        // console.log(amount)
                    }
                    dates.push(moment(record.createdOn).format("YYYY-MM-DD"))
                    console.log(record) 
                }
                setResultDate(dates) // end of if
                setTotalBalance(total)   
            })
        } // end of if 
    }, [startDate, endDate])


    return <>
        <h2 className="text-center mb-3">Balance trend</h2>

        <Form>
            <Row className="mb-5">
                <Col lg="6">
                    <h6>From</h6>
                    <Form.Control
                        type="date"
                        value={startDate}
                        onChange={(e) => setStartDate(e.target.value)}
                    />
                </Col>
                <Col lg="6">
                    <h6>To</h6>
                    <Form.Control
                        type="date"
                        value={endDate}
                        onChange={(e) => setEndDate(e.target.value)}
                    />
                </Col>
            </Row>
        </Form>

        <Line 
            data = {{
                labels: resultDate,
                datasets: [{
                    label:'Report',
                    fill: true,
                    lineTension: 0.5,
                    backgroundColor: 'rgba(75,192,192,0.2)',
                    borderColor: 'rgba(0,0,0,1)',
                    borderWidth: 2,
                    data: totalBalance
                }]
            }}
        />
        
    </>
}

export default Trend

export async function getStaticProps() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/records`)
    const data = await res.json();

    return {
        props: {
            data
        }
    }
} 
