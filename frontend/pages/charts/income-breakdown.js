import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { colorRandomizer } from '../../helpers';

const iBreakdown = ({data}) => {
    
    const [ name, setName ] = useState([]);
    const [ amount,setAmount ] = useState([]);
    const [ bgColors, setBgColors ] = useState([]);
    const [ result, setResult ] = useState('');


    useEffect(() => {
        const inc = data.filter(record => record.categoryType === "Income")
        // console.log(inc)

        setResult(inc)
        // console.log(result)

    }, [data])

    useEffect(() => {
        if(result){
            setName(result.map(element => element.categoryName))
            setAmount(result.map(element => element.amount))
            setBgColors(result.map(() => `#${colorRandomizer()}`))
        }
    }, [result])


    return <>
        <h2 className="text-center mb-3">Category Breakdown (Income)</h2>
        <div>
            <Pie 
                data={{
                    labels: name,
                    datasets: [{
                        data: amount,
                        backgroundColor: bgColors,
                        hoverBackgroundColor: bgColors
                    }]
                }}
                width={'350px'}
                height={'350px'}
                options={{ maintainAspectRatio: false }}
            />
        </div>
    </>


}
export default iBreakdown

export async function getStaticProps() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/records`)
    const data = await res.json();

    return {
        props: {
            data
        }
    }
} 