import { useState, useEffect } from 'react';
import { Form, Row, Col } from "react-bootstrap";
import { Pie } from 'react-chartjs-2';
import { colorRandomizer } from '../../helpers';

const Breakdown = ({data}) => {
    
    const [ name, setName ] = useState([]);
    const [ amount,setAmount ] = useState([]);
    const [ bgColors, setBgColors ] = useState([]);
    const [ startDate, setStartDate ] = useState('');
    const [ endDate, setEndDate ] = useState('');
    const [ resultDate, setResultDate ] = useState('');

    useEffect(() => {
        if (startDate !== '' && endDate !== '') {
            const result = data.filter(record => {
                return ( 
                    record.createdOn >= startDate && record.createdOn <= endDate
                )
            })
            setResultDate(result)
            // console.log(resultDate)
        } else {
            setResultDate(data)
        }
    }, [startDate, endDate])

    useEffect(() => {
        if(resultDate){
            setName(resultDate.map(element => element.categoryName))
            setAmount(resultDate.map(element => element.amount))
            setBgColors(resultDate.map(() => `#${colorRandomizer()}`))
        }
    }, [resultDate])

    // console.log(name)
    // console.log(amount)
    // console.log(bgColors)
    // console.log(startDate)

    return <>
        <h2 className="text-center mb-3">Category Breakdown</h2>
        <Form>
            <Row className="mb-5">
                <Col lg="6">
                    <h6>From</h6>
                    <Form.Control
                        type="date"
                        value={startDate}
                        onChange={(e) => setStartDate(e.target.value)}
                    />
                </Col>
                <Col lg="6">
                    <h6>To</h6>
                    <Form.Control
                        type="date"
                        value={endDate}
                        onChange={(e) => setEndDate(e.target.value)}
                    />
                </Col>
            </Row>
        </Form>

        <div>
            <Pie 
                data={{
                    labels: name,
                    datasets: [{
                        data: amount,
                        backgroundColor: bgColors,
                        hoverBackgroundColor: bgColors
                    }]
                }}
                width={'350px'}
                height={'350px'}
                options={{ maintainAspectRatio: false }}
            />
        </div>

    </>
    
}

export default Breakdown

export async function getStaticProps() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/records`)
    const data = await res.json();

    return {
        props: {
            data
        }
    }
} 