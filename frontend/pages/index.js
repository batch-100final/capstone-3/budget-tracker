
import  Router  from 'next/router';
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext'

const Home = () => {
    const { user } = useContext(UserContext);

    console.log(user)
    
    useEffect(() => {
        (user.id !== null) ? null : Router.push('/login')
    },[])

    return <>
        <h1>Welcome to my Low-Budget Tracker </h1>
        {/* <homeRoute /> */}
    </>
}

const homeRoute = () => {
    
    

}

export default Home
