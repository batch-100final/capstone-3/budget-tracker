// base imports
import React, { useState, useEffect } from 'react';
import Router from 'next/router';
import { Form, Button, Row, Col } from 'react-bootstrap';

const Register = () => {
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isActive, setIsActive] = useState(true);

    const register = (e) => {
        e.preventDefault();
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/email-exists`, {
            method: 'Post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data); // email exist true or false

            if(data === false){
                fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                    firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data) // true if registered annd false if error
                    if(data) {
                    setFirstName('');
					setLastName('');
					setEmail('');
					setPassword1('');
                    setPassword2('');
					setMobileNo('');

                    alert('thank you for registering')

                    Router.push('/login');
                    }
                })
            } else {
                alert('User already exist')
            }
        })
    }

    useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password1 !== '';
        let isPasswordConfirmNotEmpty = password2 !== '';
        let isPasswordMatched = password1 === password2;
        let isInfoNotEmpty = firstName && lastName && mobileNo !== '';

        if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatched && isInfoNotEmpty) {
            setIsActive(false);
        } else {
            setIsActive(true);
        }
    }, [email, password1, password2, firstName, lastName, mobileNo]);

    return <>
        <Row className="justify-content-center">
            <Col xs md="6">   
                <h2>Register</h2>
                <Form onSubmit={(e) => register(e)} className="my-4">
                    <Form.Group>
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control 
                        type="email" 
                        placeholder="e.g. Example@email.com" 
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    /> 
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Must have at least 6 characters" 
                        value={password1} 
                        onChange={(e) => setPassword1(e.target.value)}
                        required
                    /> 
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Confirm Password" 
                        value={password2} 
                        onChange={(e) => setPassword2(e.target.value)}
                        required
                    /> 
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter your First Name" 
                        value={firstName} 
                        onChange={(e) => setFirstName(e.target.value)}
                        required
                    />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter your Last Name" 
                        value={lastName} 
                        onChange={(e) => setLastName(e.target.value)}
                        required
                    />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control 
                        type="number" 
                        placeholder="Enter your contact number" 
                        value={mobileNo} 
                        onChange={(e) => setMobileNo(e.target.value)}
                        required
                    />
                    </Form.Group>
                
                        <Button className="bg-dark" type="submit" disabled={isActive} block>Submit</Button>
                    
                </Form>
            </Col>
        </Row>
    </>
}

export default Register