// base imports
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap'
import { UserProvider } from '../UserContext';

// styles imports
import '../styles/globals.css'
import '../styles/bootstrap.min.css'

// app components
import Header from '../components/Header'
import Footer from '../components/Footer'

function MyApp({ Component, pageProps }) {
    const[user, setUser] = useState({
		id: null,
		//
	})

    useEffect(() => {
		setUser({
			id: localStorage.getItem('id'),
			//
		})
	}, [])

    // function for clearing local storage upon logout
	const unsetUser = () => {
		localStorage.clear();

		setUser({
			id: null,
			//
		})
	}

    return (
        <>
            <UserProvider value={{user, unsetUser, setUser}}>
                <Header />
                <main>
                    <Container className="my-5">
                        <Component {...pageProps} />
                    </Container>
                </main>
                <Footer />
            </UserProvider>
        </>
    )
}

export default MyApp
