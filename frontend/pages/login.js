// base imports
import  Router  from 'next/router';
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

const Login = () => {
    return <>
        <Row className="justify-content-center">
            <Col xs md="6">
                <h2>Login</h2>
                <LoginForm />
            </Col>
        </Row>
    </>
}

const LoginForm = () => {
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    

    function authenticate(e) {
        e.preventDefault();
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data); // token
            
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'google-auth-error'){
                    Swal.fire('Google Auth Error!',
                     'Google authentication procedure failed.',
                      'error'
                      )
                } else if (data.error === 'login-type-error'){
                    Swal.fire('Login Type Error!',
                     'You may have registered through a different login procedure',
                    'error'
                    )
                } else if (data.error === 'does-not-exist'){
                    Swal.fire('User does not exist!',
                     'The user you are logging in does not exist',
                     'error'
                    )
                }  else if (data.error === 'incorrect-password'){
                    Swal.fire('Incorrect Password!',
                     'Your password is inccorect',
                     'error'
                    )
                }
            }
        })
    }
    
    const authenticateGoogleToken = (response) => {
        console.log(response)
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/verify-google-id-token`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }) 
        .then((response) => response.json())
        .then(data => {
            console.log(data);

            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken)
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data.id,
                //
            })
        })
        Router.push('/categories')
    }

    return <>
        <Form onSubmit={ authenticate } className="my-4">
            <Form.Group controlId="userEmail">
                <Form.Label>Email Address</Form.Label>
                <Form.Control 
                    type="email"
                    value={email} 
                    onChange={(e) => setEmail(e.target.value)}              
                    autoComplete="off"
                    placeholder="Enter your email" 
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    value={password} 
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Enter your password"
                        required
                />
            </Form.Group>

            <Button  
                className="mb-2 bg-dark"
                type="submit" 
                block
            >
                Login
            </Button>

            <GoogleLogin
                    clientId ="970085727909-e5emhfavvbih0hru35acja7e0lba70b5.apps.googleusercontent.com"
                    buttonText = "GOOGLE LOGIN"
                    onSuccess=  { authenticateGoogleToken }
                    onFailure = { authenticateGoogleToken }
                    cookiePolicy = { 'single_host_origin'}
                    className = "w-100 text-center d-flex justify-content-center"
                />
        </Form>
    </>
}

export default Login