import { useState, useEffect } from 'react';
import { Button, Row, Col, Form } from "react-bootstrap";
import Swal from 'sweetalert2';
import Router from "next/router";

const RecordForm = () => {
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const [amount, setAmount] = useState(0);
    const [description, setDescription] = useState('');
    const [categoryList, setCategoryList] = useState('');

    const addRecord = (e) => {
        e.preventDefault();
        
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/records`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            categoryName: categoryName,
            categoryType: categoryType,
            amount: amount,
            description: description
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true){
                
                Swal.fire('Sucessfully Added a New Record','',
                        'success'
                        )

            Router.push('/records')
            
            } else {
                Swal.fire('Something went wrong!',
                     'Try adding something again.',
                      'error'
                      )
            }
        })

    }

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/categories`,{
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(categoryType === "Income"){
                const categoryList = data.map(category => {
                    if(category.categoryType === "Income") {
                        return (
                            <option key={category._id}> {category.categoryName} </option>
                        )
                    }
                })
                setCategoryList(categoryList);
            }
            else if(categoryType === "Expense"){
                const categoryList = data.map(category => {
                    if(category.categoryType === "Expense") {
                        return (
                            <option key={category._id}> {category.categoryName} </option>
                        )
                    }
                })
                setCategoryList(categoryList);
            }
        })
    }, [categoryType])

    return<>
        <Row className="justify-content-center">
            <Col xs md="6" className>
            <h1>Add New Record</h1>
                <Form onSubmit={(e) => addRecord(e)} className="my-4">

                    <Form.Group>
                        <Form.Label>Category Type</Form.Label>
                        <Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
                            <option>Select Category</option>
                            <option>Income</option>
                            <option>Expense</option>
                        </Form.Control>
                    </Form.Group>
                    
                    <Form.Group>
                        <Form.Label>Category Name</Form.Label>
                        <Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)}>
                            { categoryList }
                            
                        </Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Amount</Form.Label>
                        <Form.Control 
                        type="number" 
                        placeholder="Enter amount" 
                        value={amount} 
                        onChange={(e) => setAmount(e.target.value)}
                        required
                    />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter description" 
                        value={description} 
                        onChange={(e) => setDescription(e.target.value)}
                        required
                    />
                    </Form.Group>

                    <Button className="bg-dark" type="submit" block>Submit</Button>
                </Form>
            </Col>
        </Row>
    </>
}

export default RecordForm