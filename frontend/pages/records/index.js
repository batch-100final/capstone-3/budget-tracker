import { useEffect, useState } from 'react';
import { Col, Row, Card, Button, Form } from 'react-bootstrap';
import Link from 'next/link'

const Index = ({ data }) => {
    console.log(data)
    const [ categoryType, setCategoryType] = useState('');
    const [ searchTerm, setSearchTerm] = useState('');
    const [ records, setRecords ] = useState('');
    const [ totalBalance, setTotalBalance ] = useState(0);

    let balance = 0;
    
    useEffect(() => {
        for (let record of data){
            if(record.categoryType === "Income"){
                balance = balance + record.amount
                // console.log(balance)
            } else if (record.categoryType === "Expense"){
                balance = balance - record.amount
            }
            setTotalBalance(balance)
        }
    },[totalBalance, searchTerm, categoryType])

    useEffect(() => {
        if(categoryType === "Income"){
            const recordData = data.map(record => {
                if(record.categoryName.toLowerCase().includes(searchTerm.toLowerCase())){
                    if(record.categoryType === "Income") {
                        return (
                            <Card key={record._id}>
                                <Card.Header><h4>{record.categoryName}</h4></Card.Header>
                                <Card.Body>
                                    <Card.Title><h5>{record.description}</h5></Card.Title>
                                        <Card.Text> {record.categoryType} </Card.Text>
                                        <Card.Text> {record.amount} </Card.Text>
                                </Card.Body>
                            </Card>
                        )
                    }
                }
            })
            setRecords(recordData)
        } else if(categoryType === "Expense"){
            const recordData = data.map(record => {
                if(record.categoryName.toLowerCase().includes(searchTerm.toLowerCase())){
                    if(record.categoryType === "Expense") {
                        return (
                            <Card key={record._id}>
                                <Card.Header><h4>{record.categoryName}</h4></Card.Header>
                                <Card.Body>
                                    <Card.Title><h5>{record.description}</h5></Card.Title>
                                        <Card.Text> {record.categoryType} </Card.Text>
                                        <Card.Text> {record.amount} </Card.Text>
                                </Card.Body>
                            </Card>
                        )
                    }
                }
            })
            setRecords(recordData)
        } else {
            const recordData = data.map(record => {
                if(record.categoryName.toLowerCase().includes(searchTerm.toLowerCase())){
                    return (
                        <Card key={record._id}>
                            <Card.Header><h4>{record.categoryName}</h4></Card.Header>
                            <Card.Body>
                                <Card.Title><h5>{record.description}</h5></Card.Title>
                                    <Card.Text> {record.categoryType} </Card.Text>
                                    <Card.Text> {record.amount} </Card.Text>
                            </Card.Body>
                        </Card>
                    ) 
                }
            })
            setRecords(recordData)
        }
    }, [categoryType, searchTerm])

    console.log(categoryType)

    return<>
    <h2 className="text-center">Records</h2>
        <Row className="justify-content-center">
            
            <Col xs md="6" lg="3">
                <Link href="/records/new">
                    <a><Button variant="outline-success" className="mb-3" type="submit" block>Add Records</Button></a>
                </Link>
            </Col>

            <Col xs md="6" lg="3">
                <Form.Control
                    type="text"
                    placeholder="Search Records"
                    onChange={e => setSearchTerm(e.target.value)}
                />
            </Col>

            <Col xs md="6" lg="3">
                <Form.Control
                as="select"
                onChange={(e) => setCategoryType(e.target.value)}
                >
                <option>All</option>
                <option>Income</option>
                <option>Expense</option>
                </Form.Control>
            </Col>
            <h3>Balance: {totalBalance} </h3>
        </Row>
        
        {records}

    </>
}

export default Index

export async function getStaticProps() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/records`)
    const data = await res.json();

    return {
        props: {
            data
        }
    }
} 