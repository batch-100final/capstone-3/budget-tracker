import React from 'react';

// create a context object
const UserContext = React.createContext();

// provider components allows consming components to subscribe to context
export const UserProvider = UserContext.Provider;

export default UserContext;