const Category = require('../models/category')

// add category
module.exports.add = (params) => {
    let category = new Category({
        categoryName: params.categoryName,
        categoryType: params.categoryType
    })
    return category.save().then((category, err) => {
        return(err) ? false : true
    })
}

// get all
module.exports.getAll = () => {
	return Category.find({ isActive: true }).then(categories => categories)
}