const Record = require('../models/record')

module.exports.add = (params) => {
    let record = new Record({
        categoryName: params.categoryName,
        categoryType: params.categoryType,
        amount: params.amount,
        description: params.description,
    })
    return record.save().then((record, err) => {
        return(err) ? false : true
    })
}

// get all
module.exports.getAll = () => {
	return Record.find({ isActive: true }).then(records => records)
}