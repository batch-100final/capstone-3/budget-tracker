const User = require('../models/user')
const bcrypt = require('bcryptjs')
const auth = require('../auth')
const { OAuth2Client } = require('google-auth-library')
const { TokenExpiredError } = require('jsonwebtoken')
const { defaultPlaygroundOptions } = require('apollo-server-core')
const clientId = '970085727909-e5emhfavvbih0hru35acja7e0lba70b5.apps.googleusercontent.com'

// email exists
module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

// register 
module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

// login
module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) { 
			return { error: 'does-not-exist'}
		}
		if (user.loginType !== 'email') {
			return { error: 'login-type-error'}
		}	
			
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return { error: 'incorrect-password'}
		}
	})
}

//get details
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

// google
module.exports.verifyGoogleTokenId = async (tokenId) => {
	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})
	console.log(data.payload.email_verified);

	if(data.payload.email_verified === true) {

		const user = await User.findOne({ email: data.payload.email })

		console.log(user);

		if(user !== null){
			if(user.loginType === 'google') {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			} else {
				return { error: 'login-type-error' }
			}
		} else {
			let user = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})
		
			return user.save().then((user, err) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			})
		}
	} else {
		return { error: "google-auth-error" }
	}
}