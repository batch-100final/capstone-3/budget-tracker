const mongoose = require('mongoose')

const recordSchema = new mongoose.Schema({
    categoryName: {
        type: String,
        required: [true, 'Name is required']
    },
    categoryType: {
        type: String,
        required: [true, 'Type is required']
    },
    amount: {
        type: Number,
        required: [true, 'Amount is required']
    },
    description: {
        type: String,
        required: [true, 'Description is required']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('record', recordSchema)