const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required.']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required.']
    },
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    password: {
        type: String
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    loginType: {
        type: String,
        default: 'email'
    },
    mobileNo: {
        type: String
    }
})

module.exports = mongoose.model('user', userSchema);