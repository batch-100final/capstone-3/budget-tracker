const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
    categoryName: {
        type: String 
    },
    categoryType: {
        type: String
    },
    isActive: {
        type: Boolean,
        default: true
    }

})

module.exports = mongoose.model('category', categorySchema)
