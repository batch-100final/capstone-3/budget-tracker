const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
const port = 4000;

app.use(cors())

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:rtPgDHug1rSobzDW@wdc028-course-booking.0dvhv.mongodb.net/budget_tracker?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Routes
const userRoutes = require('./routes/user')
const categoryRoutes = require('./routes/category')
const recordRoutes = require('./routes/record')

app.use('/api/users', userRoutes)
app.use('/api/categories', categoryRoutes)
app.use('/api/records', recordRoutes)

app.listen(port, () => {
    console.log(`API is now online on port ${ port }`)
})