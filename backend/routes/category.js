const express = require('express')
const router = express.Router()
const CategoryController = require('../controllers/category')
// const auth = require('../auth')

// add category
router.post('/', (req,res) => {
    CategoryController.add(req.body).then(result => res.send(result))
})

// get all category
router.get('/', (req, res) => {
    CategoryController.getAll().then(categories => res.send(categories))
})

module.exports = router