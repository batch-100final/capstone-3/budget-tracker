const express = require('express');
const router = express.Router();
const UserController = require('../controllers/user');
const auth = require('../auth');

// Primary Routes

// email-exist
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

// register
router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

// login
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

// get details
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

// google
router.post('/verify-google-id-token', async (req, res) => {
    res.send( await UserController.verifyGoogleTokenId(req.body.tokenId))
})

module.exports = router