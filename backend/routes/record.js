const express = require('express')
const router = express.Router()
const RecordController = require('../controllers/record');
// const auth = require('../auth')

// add record
router.post('/', (req,res) => {
    RecordController.add(req.body).then(result => res.send(result))
})

// get all records
router.get('/', (req, res) => {
    RecordController.getAll().then(records => res.send(records))
})

module.exports = router